function loadPage(page) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('main').innerHTML = this.responseText;
			initPage();
		}
	};

	var path = 'pages/' + page;

	xhttp.open('GET', path, true);
	xhttp.send();
} 