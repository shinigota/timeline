var menuProperties = {
	active: false,
	collapsableMenu: document.getElementById('collapsable_menu'),
	pages: {
		'button_create_entry': 'creer_article.html',
	},
	buttonsCallbacks : {
		'button_main_menu' : function() {
				console.info('event menuButton');
				toggleMenu();
		},

		'button_load_page': function(buttonId) {
			if ( buttonId === 'button_main_menu' )
				return function() {
					console.info('event ' + buttonId);
					toggleMenu();
				};
			else if ( buttonId === 'button_display_timeline' )
				return function() {
					console.info('event ' + buttonId);
					toggleMenu();
					initTimeline();
				};
			else if ( menuProperties.pages[buttonId] != null )
				return function() {
					console.info('event ' + buttonId);
					toggleMenu();
					loadPage(menuProperties.pages[buttonId]);
				};
			else
				return function() {
					console.error('no event for button id ' + buttonId);
				};
		},
	}
};

var toggleMenu = function() {
	menuProperties.active = !menuProperties.active;
	if(menuProperties.active) {
		menuProperties.collapsableMenu.style.display = 'block';
	}
	else {
		menuProperties.collapsableMenu.style.display = 'none';
	}
};

var initMenu = function() {
	var buttons = document.getElementsByClassName('topbar_action');
	Array.prototype.forEach.call(buttons, function(button) {
		button.addEventListener( 'click', menuProperties.buttonsCallbacks['button_load_page'](button.id) );
	});
};
