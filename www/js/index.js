var onDeviceReady = function() {
    loadPage('timeline.html');
};

var initApplication = function() {
    document.addEventListener('deviceready', onDeviceReady, false);
    initMenu();
    initTimeline();
};


initApplication();