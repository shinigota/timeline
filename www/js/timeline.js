function initTimeline() {
	var timeline = document.getElementById("main");
	while(timeline.firstChild){
    	timeline.removeChild(timeline.firstChild);
	}

	timeline.class = "timeline";

	var entries = loadTimeline();
	buildTimeline( timeline, entries );
	
}

function buildTimeline(timeline, entries) {
	console.info('-- timeline buildTimeline -- ');
	for ( var entryId in entries ) {
		timeline.appendChild(buildEntry(entryId, entries));
	}
}

function buildEntry(entryId, entries) {
	console.info('-- timeline buildEntry -- ');
	var entry = entries[entryId];
	console.dir(entry);
	var divContainer = document.createElement("div");
	divContainer.className = "timeline_entry";

	var title = document.createElement("h3");
	title.appendChild(document.createTextNode(formatDate(entryId) + ' ' + entry.title));
	divContainer.appendChild(title);

	if ( entry.text != null ) {
		var text = document.createElement("p");
		text.appendChild(document.createTextNode(entry.text));
		divContainer.appendChild(text);
	}

	if ( entry.picture != null ) {
		var picture = document.createElement("img");
		picture.setAttribute( "src", entry.picture );
		divContainer.appendChild(picture);
	}
	if ( entry.video != null ) {
		console.log('not null');
		var video = document.createElement("video");
		video.setAttribute( "controls", "");

		var source = document.createElement("source");
		source.setAttribute( "type", "video/mp4");
		source.setAttribute( "src", entry.video);

		video.appendChild(source);
		divContainer.appendChild(video);
	}

	console.log('geoloc');
	console.dir(entry.geolocation);
	if ( entry.geolocation != null ) {
		var geolocation = document.createElement("img");
		geolocation.setAttribute( "src", entry.geolocation);
		divContainer.appendChild(geolocation);
	}

	return divContainer;
}