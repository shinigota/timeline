function getFileContentAsBase64(path,callback){
    window.resolveLocalFileSystemURL(path, gotFile, fail);
            
    function fail(e) {
          alert('Cannot found requested file');
    }

    function gotFile(fileEntry) {
      console.log('gotfile');
           fileEntry.file(function(file) {
            console.log('zerzerze');
            console.log(file);
              var reader = new FileReader();
              reader.onloadend = function(e) {
                console.log('read');
                console.log(this.result);
                console.log(e);
                   var content = this.result;
                   callback(content);
              };
              // The most important point, use the readAsDatURL Method from the file plugin
              reader.readAsDataURL(file);
           });
    }
}

function currentTimestamp() {
  	return Math.floor(Date.now() / 1000);
}

function formatDate(timestampSec) {
  	var date = new Date(timestampSec * 1000);
  	return date.toLocaleString();
}
