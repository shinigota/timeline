var pageProperties = {
    buttonsCallbacks : {
        'button_take_picture': {
            event: function() {
                console.log('event button_take_picture');
                navigator.camera.getPicture(pageProperties.buttonsCallbacks['button_take_picture'].onSuccess, 
                                            pageProperties.buttonsCallbacks['button_take_picture'].onFail, 
                                            {   quality: 50, 
                                                destinationType: Camera.DestinationType.DATA_URL,
                                                targetWidth: 640,
                                                targetHeight: 480,
                                            }
                );
            },
            onSuccess: function(imageData) {
                var pictureContainer        = document.getElementById('picture_container');
                var picture                 = document.createElement("img");
                picture.id                  = 'picture';
                picture.src                 = "data:image/png;base64," + imageData;
                pictureContainer.appendChild(picture);

            },
            onFail: function(message) {
                alert('Failed because: ' + message);
            }
        },
        'button_take_video': {
            event: function() {
                console.log('event button_take_video');
                navigator.device.capture.captureVideo(   pageProperties.buttonsCallbacks['button_take_video'].onSuccess, 
                                                     pageProperties.buttonsCallbacks['button_take_video'].onFail,
                                                     { duration: 8 } );
            },
            onSuccess: function(mediaFiles) {
                for (var i = 0; i < mediaFiles.length; i ++) {
                    var videoFile         = mediaFiles[i];
                    var videoContainer    = document.getElementById('video_container');
                    var video             = document.createElement("video");
                    video.id              = 'video';
                    video.src             = videoFile.localURL;
                    console.log('1');
                    console.log(videoFile.localURL);
                    console.log(videoFile.fullPath);

                }
            },
            onFail: function(message) {
                alert('Failed because: ' + message);
            }
        },
        'button_get_geolocation': {
            event: function() {
                console.log('event button_get_geolocation');
                navigator.geolocation.getCurrentPosition( 
                    pageProperties.buttonsCallbacks['button_get_geolocation'].onSuccess,
                    pageProperties.buttonsCallbacks['button_get_geolocation'].onFail,
                    { enableHighAccuracy: true }
                );
            },

            onSuccess: function(position) {
                var apiKey                  = 'AIzaSyCGY2_nJ_ep8UFSeLVfQF8ygQA1v_ZNF98';
                var geolocationContainer    = document.getElementById('geolocation_container');
                var geolocation             = document.createElement("img");
                var latitude                = position.coords.latitude;
                var longitude               = position.coords.longitude;

                geolocation.src             = 'https://maps.googleapis.com/maps/api/staticmap?center=' + latitude  + ',' + longitude + '&zoom=12&size=400x400&markers=color:red|' + latitude  + ',' + longitude + '&key=' + apiKey;
                geolocation.id              = 'geolocation';
                geolocationContainer.appendChild(geolocation);
            },

            onFail: function(message) {
                alert('Failed because: ' + message);
            }
        },
        'button_validate_article': {
            event: function() {
                console.log('event button_validate_article');
                var date        = currentTimestamp();
                var title       = document.getElementById('article_title').value;
                var text        = document.getElementById('article_text').value;
                var image       = document.getElementById('picture') == null ? null : document.getElementById('picture').src;
                var geolocation  = document.getElementById('geolocation') == null ? null : document.getElementById('geolocation').src;
                var video       = null;

                addToStorage(date, title, image, video, geolocation, text);
                menuProperties.buttonsCallbacks['button_load_page']('button_display_timeline')();
            },
        },
    },
    // 'button_validate': {
    //  event: function() {
    //      console.log('validate');
    //      var title = document.getElementById("entry_title").textContent;
    //      var text = document.getElementById("entry_text").textContent;
    //      var picture = document.getElementById("entry_picture").getAttribute("src");

    //      if ( title === null ) title = "Sans titre";

    //      addToStorage(getTimestamp(), title, picture, null, null, text);
    //      // addToStorage(date, type, title, picture, video, geolocation, text)
    //  }
    // },
};

var self = this;

var initPage = function() {
    var timeline = document.getElementById("main");
    timeline.class = "";

    var buttons = document.getElementsByClassName('page_action');
    for (var i= 0; i < buttons.length; i++) {
        var button = buttons[i];
        if (pageProperties.buttonsCallbacks[button.id] == null || pageProperties.buttonsCallbacks[button.id].event == null)
            continue;
        button.addEventListener( 'click', self.pageProperties.buttonsCallbacks[button.id].event );

    }
};

        

